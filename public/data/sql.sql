-- -----------------------------
-- QeyserBlog MySQL Data Transfer 
-- 
-- Host     : 127.0.0.1
-- Port     : 3306
-- Database : Qeyserblog
-- 
-- Date : 2017-06-16 02:09:27
-- -----------------------------

SET FOREIGN_KEY_CHECKS = 0;

-- --------------------------------------------------------

--
-- 表的结构 `qeyser_admin`
--
DROP TABLE IF EXISTS `qeyser_admin`;
CREATE TABLE `qeyser_admin` (
  `uid` smallint(5) unsigned NOT NULL COMMENT '用户ID',
  `username` varchar(60) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '用户密码',
  `nickname` varchar(60) NOT NULL DEFAULT '' COMMENT '昵称',
  `email` varchar(60) NOT NULL DEFAULT '' COMMENT '邮箱地址',
  `qq` char(10) NOT NULL DEFAULT '' COMMENT 'qq号',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '注册时间',
  `last_ip` varchar(15) NOT NULL DEFAULT '0' COMMENT '最后登录IP',
  `salt` varchar(12) NOT NULL COMMENT '密码盐值',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '会员状态',
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='管理员表';

-- --------------------------------------------------------

--
-- 表的结构 `qeyser_article`
--
DROP TABLE IF EXISTS `qeyser_article`;
CREATE TABLE `qeyser_article` (
  `aid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文档ID',
  `title` varchar(150) NOT NULL DEFAULT '' COMMENT '标题',
  `cid` smallint(4) unsigned NOT NULL DEFAULT '0' COMMENT '所属分类',
  `keywords` varchar(100) NOT NULL DEFAULT '' COMMENT '关键词',
  `description` varchar(180) NOT NULL DEFAULT '' COMMENT '描述',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `pic` varchar(120) DEFAULT NULL COMMENT '缩略图',
  `content` text NOT NULL COMMENT '文档内容',
  `click` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '浏览量',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `sort` tinyint(4) unsigned NOT NULL DEFAULT '99' COMMENT '排序',
  PRIMARY KEY (`aid`),
  KEY `cid_type` (`cid`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='文章表';

-- --------------------------------------------------------

--
-- 表的结构 `qeyser_category`
--

DROP TABLE IF EXISTS `qeyser_category`;
CREATE TABLE `qeyser_category` (
  `cid` int(10) unsigned NOT NULL COMMENT '栏目ID',
  `cname` varchar(100) NOT NULL DEFAULT '' COMMENT '栏目名称',
  `keywords` varchar(100) NOT NULL DEFAULT '' COMMENT '关键词',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '备注信息',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '类型',
  `sort` tinyint(4) unsigned NOT NULL DEFAULT '99' COMMENT '排序',
  PRIMARY KEY (`cid`),
  KEY `cname` (`cname`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='文章栏目表';

--
-- 转存表中的数据 `qeyser_category`
--

INSERT INTO `qeyser_category` (`cid`, `cname`, `keywords`, `description`, `type`, `sort`) VALUES
(1, 'سىستېما بېلىملىرى', 'سېستىما', 'سىستېما بېلىملىرى يوللىنىدۇ.', 1, 1),
(2, 'يۇمتال بېلىملىرى', 'يۇمشاق دېتال', 'يۇمشاق دېتال بېلىملىىرى يوللىنىدۇ.', 1, 2),
(3, 'تور بىلىملىرى', 'تور', 'تور بىلىملىرى يوللىنىدۇ.', 1, 3),
(4, 'Office بېلىملىرى', 'Office', 'Office بېلىملىرى يوللىنىدۇ', 1, 4),
(5, 'يانفۇن بېلىملىرى', 'ئەقلى تېلىفۇن', 'ئەقلى تېلفۇن بېلىملىرى يوللىنىدۇ.', 1, 5);

-- --------------------------------------------------------

--
-- 表的结构 `qeyser_comment`
--

CREATE TABLE IF NOT EXISTS `qeyser_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '留言Id',
  `aid` int(10) unsigned NOT NULL DEFAULT '0',
  `rid` int(10) DEFAULT NULL COMMENT '留言回复时间',
  `username` varchar(32) NOT NULL COMMENT '留言评论作者',
  `email` varchar(32) NOT NULL COMMENT '留言评论邮箱',
  `website` varchar(64) DEFAULT NULL,
  `content` text NOT NULL COMMENT '留言评论内容',
  `ip` varchar(16) NOT NULL COMMENT '留言评论ip',
  `add_time` int(10) NOT NULL COMMENT '留言评论时间',
  `status` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文章评论表';
-- --------------------------------------------------------

--
-- 表的结构 `qeyser_links`
--

DROP TABLE IF EXISTS `qeyser_links`;
CREATE TABLE `qeyser_links` (
  `id` int(11) NOT NULL COMMENT '友情连接id',
  `name` varchar(64) NOT NULL COMMENT '友情链接名称',
  `url` varchar(128) NOT NULL COMMENT '友情连接地址',
  `type` tinyint(1) unsigned DEFAULT '1' COMMENT '1显示、2不显示',
  `time` int(11) NOT NULL COMMENT '添加时间',
  `sort` tinyint(3) unsigned DEFAULT '99' COMMENT '排序',
  PRIMARY KEY (`id`),
  KEY `url` (`url`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='友情连接表';

--
-- 转存表中的数据 `qeyser_links`
--

INSERT INTO `qeyser_links` (`id`, `name`, `url`, `type`, `time`, `sort`) VALUES
(1, 'نۇرقۇت دۇنياسى', 'http://www.nurqut.com/', 1, 1430238360, 4),
(2, 'ئالىم ئەھەت بىلوگى', 'http://www.dolanlik.com/', 1, 1430238360, 3),
(3, 'قەيسەر بىلوگى', 'http://www.qeyser.net/', 1, 1430238360, 1),
(4, 'ئويغان بىلوگى', 'http://www.oyghanbeg.com/', 1, 1430238360, 2),
(5, 'ئارتۇچ بىلوگى', 'http://www.artuq.me/', 1, 1430238360, 5),
(6, 'مۇئەللىم بىلوگى', 'http://www.muellim.me/', 1, 1430238360, 6),
(7, 'ئۇيغۇربەگ بىلوگى', 'https://www.uyghurbeg.cn/', 1, 1430238360, 7),
(8, 'دولان تېخنىكىسى', 'http://www.xjdtg.me/', 1, 1430238360, 8);

-- --------------------------------------------------------

--
-- 表的结构 `qeyser_site`
--

DROP TABLE IF EXISTS `qeyser_site`;
CREATE TABLE `qeyser_site` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置名称',
  `title` varchar(60) NOT NULL DEFAULT '' COMMENT '配置说明',
  `value` text COMMENT '配置值',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `des` varchar(100) NOT NULL DEFAULT '' COMMENT '配置说明',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='网站配置表';

--
-- 转存表中的数据 `qeyser_site`
--

INSERT INTO `qeyser_site` (`id`,`name`, `title`, `value`, `sort`, `des`) VALUES
(1,'site_name', 'بىلوگ نامى', 'قەيسەر بىلوگى', 1, 'قەيسەر بىلوگى'),
(2,'site_title', 'بىلوگ تېمىسى', 'كومپيوتېر ساھەسىدىكى يېڭىلىقلار ۋە بىلىملەر جەمگاھى', 2, 'كومپيوتېر ساھەسىدىكى يېڭىلىقلار ۋە بىلىملەر جەمگاھى'),
(3,'site_keywords', 'ئاچقۇچلۇق سۆز', 'قەيسەر،بىلوگ،qeyser', 3, 'قەيسەر،بىلوگ،qeyser'),
(4,'site_description', 'تونۇشتۇرلىشى', 'كومپيوتېر ساھەسىدىكى يېڭىلىقلار ۋە بىلىملەر جەمگاھى', 4, 'كومپيوتېر ساھەسىدىكى يېڭىلىقلار ۋە بىلىملەر يوللىنىدۇ.'),
(5,'site_closed', 'بىلوگ ھالىتى', '1', 5, 'قىممىتى 1 بولغاندا نۇرمال، 0 بولغاندا تاقالغان ھالەت'),
(6,'default_template', 'بىلوگ ئۇسلۇبى', 'default', 6, 'بىلوگ ئۇسلۇب نامىنى يېزىڭ'),
(7,'index_page', 'باش بەت سانى', '3', 7, 'باش بەتتىكى يازما سانى'),
(8,'cate_page', 'تۈر بەتتىكى سانى', '5', 8, 'تۈر بەتتىكى يازما سانى'),
(9,'email', 'ئېلخەت ئادرىسى', 'your@domain.com', 9, 'your@domain.com'),
(10,'mail_service', 'ئېلخەت مۇلازىمىتېر', '0', 10, ''),
(11,'mail_host', 'مۇلازىمىتېر ئادرىسى', 'smtp.domain.com', 11, 'smtp.domain.com'),
(12,'mail_port', 'مۇلازىمىەت ئېغىزى', '25', 12, '25'),
(13,'mail_ssl', 'ssl نۇمۇرى', '0', 13, ''),
(14,'mail_username', 'ئېلخەت يوللىغۇچى', 'قەيسەر مۇتەللىپ', 14, 'قەيسەر مۇتەللىپ'),
(15,'mail_password', 'يوللاش پارولى', '123456', 15, '********'),
(16,'blog_version', 'بىلوگ نەشىرى', 'v1.6  20170724', 16, 'v1.0  20170618'),
(17,'icp', 'ئەن نۇمۇرى', 'QQ群: 6364475', 17, '新ICP备12007941号'),
(18,'tel', 'يانفۇن نۇمۇرى', '0998-7845106', 18, '0998-7845100'),
(19,'qq', 'QQ نۇمۇرى', '125790757', 19, '125790757');
